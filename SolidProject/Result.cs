﻿namespace SolidProject;

public enum Result
{
    Win,
    Lose,
    NeedMore,
    NeedLess
}