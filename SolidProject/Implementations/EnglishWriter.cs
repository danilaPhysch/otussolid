﻿using SolidProject.Interfaces;

namespace SolidProject.Implementations;

public class EnglishWriter : IWriter
{
    public virtual void SendInvitation() =>
        Console.WriteLine("Enter a number");

    public virtual void SendWin() =>
        Console.WriteLine("You win");

    public virtual void SendLose() =>
        Console.WriteLine("Tou lose");

    public virtual void SendNeedMore() =>
        Console.WriteLine("Need more");

    public virtual void SendNeedLess() =>
        Console.WriteLine("Need less");

    public virtual void SendCheating() =>
        Console.WriteLine("You cannot enter exactly the number. Goodbye");

    public virtual void SendLast(int attemptCount) =>
        Console.WriteLine($"You have {attemptCount} attempts left");
}