﻿namespace SolidProject.Implementations;

public class RussianWriter : EnglishWriter
{
    public override void SendInvitation() =>
        Console.WriteLine("Введи чиселко");

    public override void SendWin() =>
        Console.WriteLine("Ты победил");

    public override void SendLose() =>
        Console.WriteLine("Ты проиграл");

    public override void SendNeedMore() =>
        Console.WriteLine("Нужно побольше");

    public override void SendNeedLess() =>
        Console.WriteLine("Нужно поменьше");

    public override void SendCheating() =>
        Console.WriteLine("Нужно было вводить именно чиселко");

    public override void SendLast(int attemptCount) =>
        Console.WriteLine($"У тебя осталось попыток: {attemptCount}");
}