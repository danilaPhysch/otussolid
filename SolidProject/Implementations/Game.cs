﻿using SolidProject.Interfaces;

namespace SolidProject.Implementations;

// Single Responsibility Principle. Я мог бы сюда добавить ещё работу с сасмим числами.
// Но решил, что пусть за это будет ответственнен отдельный класс DigitsWorker.
public class Game : GameBase
{
    private readonly IDigitsWorker _digitsWorker;
    private readonly int _attemptsCount;
    private int _attemptNumber;

    public Game(IDigitsWorker digitsWorker, int attemptsCount)
    {
        _digitsWorker = digitsWorker ?? throw new ArgumentNullException(nameof(digitsWorker));

        ArgumentOutOfRangeException.ThrowIfNegativeOrZero(attemptsCount);
        _attemptsCount = attemptsCount;
    }

    public override Result MakeAttempt(int inputDigit)
    {
        _attemptNumber++;

        return _attemptNumber < _attemptsCount
            ? _digitsWorker.CompareDigits(inputDigit)
            : Result.Lose;
    }

    public override int GetAttemptsCount() =>
        _attemptsCount - _attemptNumber;
}