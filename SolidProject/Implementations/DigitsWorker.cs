﻿using SolidProject.Interfaces;

namespace SolidProject.Implementations;

public class DigitsWorker : IDigitsWorker
{
    private readonly int _digit;

    public DigitsWorker(int digitRange)
    {
        ArgumentOutOfRangeException.ThrowIfNegativeOrZero(digitRange);
        _digit = new Random().Next(digitRange);
    }

    public Result CompareDigits(int inputDigit)
    {
        if (inputDigit == _digit)
            return Result.Win;

        return inputDigit > _digit
            ? Result.NeedLess
            : Result.NeedMore;
    }
}