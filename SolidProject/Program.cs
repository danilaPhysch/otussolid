﻿using Microsoft.Extensions.Configuration;
using SolidProject;
using SolidProject.Implementations;
using SolidProject.Interfaces;

var config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .AddEnvironmentVariables()
    .Build();

var digitRange = config.GetRequiredSection("DigitRange").Get<int>();
var repeatingCount = config.GetRequiredSection("RepeatingCount").Get<int>();

// Dependency Injection
IDigitsWorker digitsWorker = new DigitsWorker(digitRange);
GameBase gameBase = new Game(digitsWorker, repeatingCount);
IWriter writer = new RussianWriter();

while (true)
{
    writer.SendLast(gameBase.GetAttemptsCount());

    writer.SendInvitation();

    if (!int.TryParse(Console.ReadLine(), out var userDigit))
    {
        writer.SendCheating();
        Environment.Exit(0);
    }

    switch (gameBase.MakeAttempt(userDigit))
    {
        case Result.Win:
            writer.SendWin();
            Environment.Exit(0);
            break;
        case Result.Lose:
            writer.SendLose();
            Environment.Exit(0);
            break;
        case Result.NeedMore:
            writer.SendNeedMore();
            break;
        case Result.NeedLess:
            writer.SendNeedLess();
            break;
        default:
            throw new ArgumentOutOfRangeException();
    }
}
