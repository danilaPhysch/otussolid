namespace SolidProject.Interfaces;

public interface INeedMore
{
    void SendNeedMore();
}