namespace SolidProject.Interfaces;

public interface IInvitation
{
    void SendInvitation();
}