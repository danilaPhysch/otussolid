﻿namespace SolidProject.Interfaces;

public interface IWriter: IInvitation, IWin, ILose, INeedMore, INeedLess, ICheating, ILast;