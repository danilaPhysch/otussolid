﻿namespace SolidProject.Interfaces;

public interface IDigitsWorker
{
    Result CompareDigits(int inputDigit);
}