namespace SolidProject.Interfaces;

public interface ILast
{
    void SendLast(int attemptCount);
}