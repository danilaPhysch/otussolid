﻿namespace SolidProject.Interfaces;

public abstract class GameBase
{
    public abstract Result MakeAttempt(int inputDigit);
    public abstract int GetAttemptsCount();
}